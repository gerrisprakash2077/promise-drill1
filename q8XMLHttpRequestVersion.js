// 8. Use this data endpoint to get the data and console the each house names and handle the error as well.
//   [ENDPOINT](https://raw.githubusercontent.com/nnnkit/json-data-collections/master/got-houses.json)

//     - Use fetch to get data.
//     - Handle if the user is not connected to internet.
//     - Handle error that may occure while fetching data.





const XMLHttpRequest = require('xhr2');

let xmlHttpR = new XMLHttpRequest();
//console.log(xmlHttpR);
xmlHttpR.open('GET', 'https://raw.githubusercontent.com/nnnkit/json-data-collections/master/got-houses.json');
xmlHttpR.onload = () => {
    let data = xmlHttpR.response
    console.log(JSON.parse(data).houses.map(ele => ele.name));
}
xmlHttpR.send();
// xmlHttpR.onload(() => {
    // console.log(xmlHttpR.response);
// })