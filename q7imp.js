// 7. Write a basic implementation of `Promise.all` that accepts an array of promises and return another array with the data 
// coming from all the promises. Make sure if any of the Promise gets rejected throw error. 
// Only when all the promises are fulfilled resolve the promise.

let promise1 = new Promise((resolve, reject) => {
    resolve('promise1 resolved');
})
let promise2 = new Promise((resolve, reject) => {
    resolve('promise2 resolved');
})
let promise3 = new Promise((resolve, reject) => {
    resolve('promise3 resolved');
})
let promise4 = new Promise((resolve, reject) => {
    resolve('promise4 resolved');
})
let promisesArray = [promise1, promise2, promise3, promise4];
//console.log(promisesArray);
function allPromises(arr, cb) {
    let resolveCollector = [];
    arr.forEach(element => {
        element.then((data) => {
            cb(null, data)
        })
        element.catch((err) => {
            cb(err)
        })
    });
}
allPromises(promisesArray, (err, data) => {
    console.log(data);
})