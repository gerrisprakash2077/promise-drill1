//6. Write a funtion named `wait` that accepts `time` in ms and executes the function after the given time.

//wait
function wait(n) {
    return new Promise((resolve, reject) => {
        setTimeout(resolve, n * 1000);
    })
}


wait(3).then(() => {
    console.log('hello');
})