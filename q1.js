promise1 = new Promise((resolve, reject) => {
    setTimeout(() => {
        resolve(`Promise Resolved!`);
    }, 2000)
})
promise1.then((resolve) => {
    console.log(resolve);
})