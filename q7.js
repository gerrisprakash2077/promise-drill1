// 7. Write a basic implementation of `Promise.all` that accepts an array of promises and return another array with the data 
// coming from all the promises. Make sure if any of the Promise gets rejected throw error. 
// Only when all the promises are fulfilled resolve the promise.

let promise1 = new Promise((resolve, reject) => {
    resolve('promise1 resolved');
})
let promise2 = new Promise((resolve, reject) => {
    resolve(new Error('promise2 rejected'));
})
let promise3 = new Promise((resolve, reject) => {
    resolve('promise3 resolved');
})
let promise4 = new Promise((resolve, reject) => {
    resolve(new Error('promise4 rejected'));
})
let promisesArray = [promise1, promise2, promise3, promise4];
//console.log(promisesArray);
Promise.all(promisesArray).then((data) => {
    console.log(data);
}).catch((rej) => {
    console.log(rej);
})